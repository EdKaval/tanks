package tankServer

type pg struct {
	Tanks   map[string]*tank
	Walls   map[int]vec4
	LineCnt int
}

func (self *pg) update(dt float64) {
	for _, value := range self.Tanks {
		value.Update(dt)
	}
}

func pgNew() pg {
	t := make(map[string]*tank)
	w := make(map[int]vec4)

	p := pg{Tanks: t, Walls: w, LineCnt: 0}

	return p
}

func (self *pg) addWall(x1 float64, y1 float64, x2 float64, y2 float64) {
	self.LineCnt++
	self.Walls[self.LineCnt-1] = vec4{x1, y1, x2, y2}
}

func (self *pg) updateNet() {

}
