package tankServer

import "fmt"
import "net"

var PlayBox pg

func PointInBox(a vec2, rect vec4) bool {
	if rect.x > rect.z {
		rect.x, rect.z = rect.z, rect.x
	}
	if rect.y > rect.w {
		rect.y, rect.w = rect.w, rect.y
	}

	if a.x >= rect.x && a.y >= a.y && a.x <= rect.z && a.y <= rect.w {
		return true
	} else {
		return false
	}
}

func Intersection(line1 vec4, line2 vec4) (vec2, bool) {
	d := (line1.x-line1.z)*(line2.y-line2.w) - (line1.y-line1.w)*(line2.x-line2.z)
	a := line1.x*line1.w - line1.y*line1.z
	b := line2.x*line2.w - line2.y*line2.z
	x := (a*(line2.x-line2.z) - b*(line1.x-line1.z)) / d
	y := (a*(line2.y-line2.w) - b*(line1.y-line1.w)) / d

	p := vec2{x, y}
	if PointInBox(p, line1) && PointInBox(p, line2) {
		return p, true
	} else {
		return p, false
	}
}

func Receiver() {
	pc, err := net.ListenPacket("udp", "localhost:27236")
	if err != nil {
		return
	}

	for {
		buf := make([]byte, 100)
		len, addr, err := pc.ReadFrom(buf)
		if err != nil {
			fmt.Printf("Error\n")
			continue
		}

		go Handler(buf, len, addr.String())

	}
}

func Handler(buf []byte, len int, addr string) {

	_, ok := PlayBox.Tanks[addr]

	if !ok {
		go RegisterNewTank(buf, len, addr)
		return
	}

	tnk := PlayBox.Tanks[addr]

	str := string(buf)

	var time, x, y, angle, angSpeed, speed float64

	fmt.Sscanf(str, "%f %f %f %f %f %f", &time, &x, &y, &angle, &angSpeed, &speed)

	if (*tnk).lastPacket < time {
		(*tnk).lastPacket = time
		(*tnk).coords.x = x
		(*tnk).coords.y = y
		(*tnk).angle.x = angle
		(*tnk).SetRotSpeed(angSpeed)
		(*tnk).SetSpeed(speed)
		//fmt.Printf("Addr: %s Time: %f: Tank at x:%f y:%f angle: %f\n", addr, time, (*tnk).coords.x, (*tnk).coords.y, (*tnk).angle.x)
	}
}

func RegisterNewTank(buf []byte, len int, addr string) {
	PlayBox.Tanks[addr] = NewTank()

	tnk := PlayBox.Tanks[addr]

	str := string(buf)

	var time, x, y, angle, angSpeed, speed float64

	fmt.Sscanf(str, "%f %f %f %f %f %f", &time, &x, &y, &angle, &angSpeed, &speed)

	(*tnk).coords = vec2{x: x, y: y}
	(*tnk).SetSpeed(speed)
	(*tnk).SetRotSpeed(angSpeed)
	(*tnk).hp = 200
	(*tnk).lastPacket = time
	(*tnk).angle.x = angle

	fmt.Printf("Registered tank with address: %s at x:%f y:%f angle: %f, hp: %f\n", addr, (*tnk).coords.x, (*tnk).coords.y, (*tnk).angle.x, (*tnk).hp)
}

func MainLoop() {
	PlayBox = pgNew()

	Receiver()

}
