package tankServer

type vec2 struct {
	x float64
	y float64
}

type vec3 struct {
	x float64
	y float64
	z float64
}

type vec4 struct {
	x float64
	y float64
	z float64
	w float64
}

type line struct {
	start vec2
	end   vec2
}
