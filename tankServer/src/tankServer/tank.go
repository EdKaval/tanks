package tankServer

//import "net"
import "math"

type tank struct {
	coords     vec2
	size       float64
	speed      vec2 //speed, maxSpeed
	angle      vec3 //angle, rotation speed, max rotation speed
	hp         float64
	wait       bool
	lastPacket float64
}

func (self *tank) SetSpeed(newSpeed float64) {
	self.speed.x = math.Max(math.Min(newSpeed, self.speed.y), -self.speed.y)
}

func (self *tank) SetRotSpeed(newSpeed float64) {
	self.angle.y = math.Max(math.Min(newSpeed, self.angle.z), -self.angle.z)
}

func (self *tank) UpdateNet(buf []byte) {

}

func (self *tank) Update(dt float64) {
	cosin := math.Cos(self.angle.x)
	sinus := math.Sin(self.angle.x)

	self.coords.x = self.coords.x + cosin*self.speed.x*dt
	self.coords.y = self.coords.y + sinus*self.speed.x*dt
	self.angle.x = self.angle.x + self.angle.y*dt
}

func (self *tank) SendStatus(addr string) {

}

func NewTank() *tank {
	tnk := tank{coords: vec2{0, 0}, size: 0.05, speed: vec2{0, 500}, angle: vec3{0, 0, 5}, hp: 100}

	return &tnk
}
