walls = {}
walls.__index = walls

function walls:new()
	self = setmetatable({}, self)

	self.walls = {}

	return self
end

function walls:addWall(x1, y1, x2, y2)
	wall = vec4(plen(x1), plen(y1), plen(x2), plen(y2))

	table.insert(self.walls, wall)
end

function walls:draw()
	for key, value in pairs(self.walls) do
		love.graphics.line(value.x, value.y, value.z, value.w)
	end
end

function walls:collisionCheck(tank)
	local p1, p2, p3, p4 = tank:getPoints()

	for key, value in pairs(self.walls) do
		local w1 = {x = value.x, y = value.y}
		local w2 = {x = value.z, y = value.w}

		local x, y = intersection(p1, p2, w1, w2)
		if x then return true end
		local x, y = intersection(p2, p3, w1, w2)
		if x then return true end
		local x, y = intersection(p3, p4, w1, w2)
		if x then return true end
		local x, y = intersection(p1, p4, w1, w2)
		if x then return true end
	end
	return false
end
