ffi = require("ffi")
sock = require("socket")
bit = require("bit")
require("tank")
require("walls")
require("bullet")

---------------CDEF------------------------------------------------

ffi.cdef[[
typedef struct {
	double x, y;
} vector2;

typedef struct {
	double x, y, z;
} vector3;

typedef struct {
	double x, y, z, w;
} vector4;
]]

vec2 = nil
vec3 = nil
vec4 = nil

local vect2 = {
	__add = function(self, next)
		return vec2(self.x + next.x, self.y + next.y)
	end,
	__sub = function(self, next)
		return vec2(self.x - next.x, self.y - next.y)
	end,
	__mul = function(self, num)
		return vec2(self.x*num, self.y*num)
	end,
}

local vect3 = {
	__add = function(self, next)
		return vec3(self.x + next.x, self.y + next.y, self.z + next.z)
	end,
	__sub = function(self, next)
		return vec3(self.x - next.x, self.y - next.y, self.z - next.z)
	end,
	__mul = function(self, num)
		return vec3(self.x*num, self.y*num, self.z*num)
	end
}

local vect4 = {
	__add = function(self, next)
		return vec4(self.x + next.x, self.y + next.y, self.z + next.z, self.w + next.w)
	end,
	__sub = function(self, next)
		return vec4(self.x - next.x, self.y - next.y, self.z - next.z, self.w - next.w)
	end,
	__mul = function(self, num)
		return vec4(self.x*num, self.y*num, self.z*num, self.w*num)
	end
}

function getByte(bn, dat)
	return bit.rshift(bit.band(dat, bit.lshift(0xff, bn*8)), bn*8)
end

function toDGram(x)  --to LittleEndian Datagram/string
	x = floor(x*1000)
	local buf = ""
	for i = 0, 7 do
		local sym = getByte(i, x)
		buf = buf .. string.char(sym)
	end
	return buf
end

function fromDGram(buf)  --from LittleEndiang Datagram/string

end

vec2 = ffi.metatype("vector2", vect2)
vec3 = ffi.metatype("vector3", vect3)
vec4 = ffi.metatype("vector4", vect4)

-------------------------------------------------------------------

function pcoords(fx, fy)    --real coordinates to pixel coordinates
	local px = fx*screenHeight
	local py = fy*screenHeight
	return px, py
end

function fcoords(px, py)    --pixel coordinates to real coordinates
	local fx = px/screenHeight
	local fy = py/screenHeight
	return fx, fy
end

function plen(fval)
	local pval = fval*screenHeight
	return pval
end

function flen(pval)
	local fval = pval/screenHeight
	return fval
end

function imageProps(height, img)
	local wid, hig = img:getDimensions()
	local scal = plen(height)/hig
	local fwid, fhig = flen(wid*scal), flen(hig*scal)

	return scal, fwid, fhig
end

function getAng(x0, y0, x1, y1, x2, y2)
	if x1 == 0 and y1 == 0 then
		x1 = math.random(-1.00,1.00)
		y1 = math.random(-1.00,1.00)
	end
	if x2 == nil or y2 == nil then
		return nil
	end
	local a = getDist(x1+x0,y1+y0,x2,y2)
	local b = getDist(x0,y0,x1+x0,y1+y0)
	local c = getDist(x0,y0,x2,y2)
	local ang = math.acos( (b^2 + c^2 - a^2 ) / (2 * b * c))
	return ang

end

function pointInBox(tp, p1, p2)
	if p1.x > p2.x then p1.x, p2.x = p2.x, p1.x end
	if p1.y > p2.y then p1.y, p2.y = p2.y, p1.y end

	if tp.x >= p1.x and tp.y >= p1.y and tp.x <= p2.x and tp.y <= p2.y then
		return true
	else
		return false
	end
end

function intersection (s1, e1, s2, e2)
	local d = (s1.x - e1.x) * (s2.y - e2.y) - (s1.y - e1.y) * (s2.x - e2.x)
	local a = s1.x * e1.y - s1.y * e1.x
	local b = s2.x * e2.y - s2.y * e2.x
	local x = (a * (s2.x - e2.x) - (s1.x - e1.x) * b) / d
	local y = (a * (s2.y - e2.y) - (s1.y - e1.y) * b) / d

	if pointInBox({x = x, y = y}, s1, e1) and pointInBox({x = x, y = y}, s2, e2) then
		return x, y
	else
		return nil, nil
	end
end

cos = math.cos
sin = math.sin
max = math.max
min = math.min
floor = math.floor

function love.keypressed(key, scancode, isrepeat)
	if key == "w" then
		tank1:setSpeed(200)
	elseif key == "s" then
		tank1:setSpeed(-200)
	end

	if key == "d" then
		tank1:setRotationSpeed(2)
	elseif key == "a" then
		tank1:setRotationSpeed(-2)
	end
end

function love.keyreleased(key)
	if key == "w" or key == "s" then
		tank1:setSpeed(0)
	end

	if key == "a" or key == "d" then
		tank1:setRotationSpeed(0)
	end
end

function love.load()
	love.window.setMode(1280, 720)
	screenWidth, screenHeight = love.window.getMode()
	tankImage = love.graphics.newImage("tank.png")

	----------------------------------
	conn = sock.udp()
	conn:settimeout(0)
	conn:setpeername("127.0.0.1", 27236)

	----------------------------------

	tank1 = tank:new(0.5, 0.5)
	Walls = walls:new()

	Walls:addWall(0.1, 0.1, 0.1, 1)
	Walls:addWall(0.5, 0.11, 1, 0.11)
end

function love.update(dt)
	tank1:update(dt, conn)
end

function love.draw()
	tank1:draw()
	Walls:draw()
	Walls:collisionCheck(tank1)
end
