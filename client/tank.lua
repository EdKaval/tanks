tank = {}
tank.__index = tank

function tank:new(x, y)
	x, y = pcoords(x, y)
	self = setmetatable({}, self)

	self.width, self.height = pcoords(0.05, 0.05)

	self.coords = vec2(x, y)
	self.speed = vec2(0, 500)  --current, max speed
	self.angle = vec3(0, 0, 5) --angle, speed, max speed

	self.image = tankImage
	self.scale = imageProps(0.05, tankImage)

	self.coords = self.coords - vec2(self.width/2, self.height/2)

	self.time = 0

	self.points = {}
	self.points[1] = {x = self.coords.x - self.width, y = self.coords.y - self.height}
	self.points[2] = {x = self.coords.x + self.width, y = self.coords.y - self.height}
	self.points[3] = {x = self.coords.x + self.width, y = self.coords.y + self.height}
	self.points[4] = {x = self.coords.x - self.width, y = self.coords.y + self.height}

	return self
end

function tank:shoot()

end

function tank:update(dt, conn)
	self.time = self.time + dt

	local cosin = cos(self.angle.x)
	local sinus = sin(self.angle.x)
	--self.coords.x = self.coords.x + cosin*self.speed*dt
	--self.coords.y = self.coords.y + sinus*self.speed*dt

	self.coords = self.coords + vec2(cosin*self.speed.x*dt, sinus*self.speed.x*dt)

	self.points[1] = {x = self.coords.x - self.width/2*cosin + self.height/2*sinus, y = self.coords.y - self.width/2*sinus - self.height/2*cosin}
	self.points[2] = {x = self.coords.x + self.width/2*cosin + self.height/2*sinus, y = self.coords.y + self.width/2*sinus - self.height/2*cosin}
	self.points[3] = {x = self.coords.x + self.width/2*cosin - self.height/2*sinus, y = self.coords.y + self.width/2*sinus + self.height/2*cosin}
	self.points[4] = {x = self.coords.x - self.width/2*cosin - self.height/2*sinus, y = self.coords.y - self.width/2*sinus + self.height/2*cosin}

	self.angle.x = self.angle.x + self.angle.y*dt

	if Walls:collisionCheck(self) then
		self.speed.x = 0
		self.angle.y = 0
		self.angle.x = self.angle.x - self.angle.y*dt
		--self.coords.x = self.coords.x - cosin*self.speed*dt
		--self.coords.y = self.coords.y - sinus*self.speed*dt
		self.coords = self.coords - vec2(cosin*self.speed.x*dt, sinus*self.speed.x*dt)
	end

	--[[
	local coordsDGram = "" .. toDGram(self.coords.x) .. toDGram(self.coords.y)
	local angleDGram = "" .. toDGram(self.angle.x) .. toDGram(self.angle.y) .. toDGram(self.angle.z)
	local speedDGram = "" .. toDGram(self.speed.x) .. toDGram(self.speed.y)

	local dg = "" .. toDGram(self.time) .. coordsDGram .. angleDGram .. speedDGram .. toDGram(200)
	--print(self.time)
	]]

	local dg = string.format("%f %f %f %f %f %f", self.time, flen(self.coords.x), flen(self.coords.y), self.angle.x, self.angle.y, self.speed.x)
	conn:send(dg)
end

function tank:setSpeed(sp)
	self.speed.x = max(min(sp, self.speed.y), -self.speed.y)
end

function tank:setRotationSpeed(sp)
	self.angle.y = max(min(sp, self.angle.z), -self.angle.z)
end

function tank:draw()
	local x = self.coords.x - self.width/2*cos(self.angle.x) + self.height/2*sin(self.angle.x)
	local y = self.coords.y - self.width/2*sin(self.angle.x) - self.height/2*cos(self.angle.x)

	love.graphics.draw(self.image, x, y, self.angle.x, self.scale)
end

function tank:getPoints()
	return self.points[1], self.points[2], self.points[3], self.points[4]
end
